package rechteck;
import java.awt.Point;

public class Testrechteck {

	public static void main(String[] args) {
		Rechteck RE = new Rechteck(20, 12, new Point(20,12));
		
		System.out.println("Kante A: " + RE.getA() + " Kante B: " + RE.getB());
		System.out.println("Diagonale: " + RE.getDiagonale());
		System.out.println("Fl�che: " + RE.getFlaeche());
		System.out.println("Umfang: " + RE.getUmfang());
	}

}
