package rechteck;
import java.awt.Point;

public class Rechteck {
	private double a;
	private double b;
	private Point point;
	
	public Rechteck(double a, double b, Point point) {
		setA(a);
		setB(b);
		setMittelpunkt(point);
	}
	
	//Setsies
	public void setA(double a) {
		if(a < 0) {
			this.a = 0;
		}else {
			this.a = a;
		}
	}
	
	public void setB(double b) {
		if(b < 0) {
			this.b = 0;
		}else {
			this.b = b;
		}
	}
	
	public void setMittelpunkt(Point point) {
		this.point = point;
	}
	
	//Getsies
	public double getA() {
		return this.a;
	}
	
	public double getB() {
		return this.b;
	}
	
	public Point getMittelpunkt() {
		return this.point;
	}
	
	//Toolz
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.a, 2) + Math.pow(this.b, 2));
	}
	
	public double getFlaeche() {
		return this.a * this.b;
	}
	
	public double getUmfang() {
		return this.a*2 + this.b*2;
	}

}//class
