package angestelltenverwaltung;

public class Angestellter {
	
	//Attribute - Deklariert
	private String name;
	private double gehalt;
	
	//Konstruktor 1 - Parameterlos
	public Angestellter() {
		
	}
	//Konstrukter 2
	public Angestellter(String name) {
		this.name = name;
		//this.gehalt = 0; standard wert eines doubles = 0 also redundant
	}
	//Konstruktor 3
	public Angestellter(String name, double gehalt) {
		this.name = name;
		this.gehalt = gehalt;
	}
	
	//Methoden
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}
	
	public double getGehalt() {
		return this.gehalt;
	}

}//class