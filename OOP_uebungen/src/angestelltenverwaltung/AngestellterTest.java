package angestelltenverwaltung;

public class AngestellterTest {
	
	//--------------Hauptprogramm--------------
	public static void main(String[] args) {
		
		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter();
		
		//Setzen der Attribute
		ang1.setName("Meier");
		ang2.setGehalt(4500);
		ang2.setName("Petersen");
		ang2.setGehalt(6000);
		
		//Bildschirmausgabe
		System.out.println("Name: " + ang1.getName());
		System.out.println("Gehalt: " + ang1.getGehalt());
		System.out.println("");
		System.out.println("Name: " + ang2.getName());
		System.out.println("Gehalt: " + ang2.getGehalt());
		
	}//main
}//class
