package tests;
import omega.*;

public class SpaceshipTest {

	public static void main(String[] args) {
		
		//String name, int energy, int shields, int lifeSupport, int hull, int torpedos, int repairAndroids
		Spaceship klingonen = new Spaceship("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		klingonen.addCargo(new Cargo("Feerengin Schneckensaft", 200));
		klingonen.addCargo(new Cargo("Bat'leth Klingonen Schwert", 200));
		
		Spaceship romulaner = new Spaceship("IRW Khazara", 100, 100, 100, 100, 2, 2);
		romulaner.addCargo(new Cargo("Borg-Schrott", 5));
		romulaner.addCargo(new Cargo("Rote Materie", 2));
		romulaner.addCargo(new Cargo("Plasma-Waffe", 50));
		
		Spaceship vulkanier = new Spaceship("Ni'Var", 80, 80, 100, 50, 0, 5);
		vulkanier.addCargo(new Cargo("Forschungssonde", 35));
		vulkanier.addCargo(new Cargo("Photonentorpedo", 3));
		
		klingonen.shootTorpedo(romulaner);
		romulaner.shootCannons(klingonen);
		vulkanier.sendBroadcast("ey ey ey Jungs........... Gewalt ist keine L�sung");
		klingonen.statusReport();
		klingonen.readCargoManifest();
		klingonen.shootTorpedo(romulaner);
		klingonen.shootTorpedo(romulaner);
		System.out.println();
		System.out.println();
		klingonen.statusReport();
		System.out.println();
		klingonen.readCargoManifest();
		System.out.println();
		System.out.println();
		romulaner.statusReport();
		System.out.println();
		romulaner.readCargoManifest();
		System.out.println();
		System.out.println();
		vulkanier.statusReport();
		System.out.println();
		vulkanier.readCargoManifest();
		
	}//main

}//class
