package omega;

public class Cargo {
	
	private String term;
	private int amount;
	
	//konstruktoren
	public Cargo() {
		this.term = "-empty-";
		this.amount = 0;
	}
	
	public Cargo(String term, int amount) {
		setTerm(term);
		setAmount(amount);
	}
	
	//Setter
	public void setTerm(String term) {
		this.term = term;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	//Getter
	public String getTerm() {
		return this.term;
	}
	
	public int getAmount() {
		return this.amount;
	}

}//class
