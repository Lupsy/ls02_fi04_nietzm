package omega;
import java.util.ArrayList;

public class Spaceship {
	
	//Variablen
	private String name;
	private int energy;
	private int shields;
	private int lifeSupport;
	private int hull;
	private int torpedos;
	private int repairAndroids;
	private ArrayList<String> broadcastCommunicator;
	private ArrayList<Cargo> cargoManifest;
	
	//Default Konstruktor
	public Spaceship() {
		//haha nicht initialisiert
		setBroadcastCommunicator(new ArrayList<String>());
		setCargoManifest(new ArrayList<Cargo>());
	}
	
	//Full Konstruktor (mit Setters)
	public Spaceship(String name, int energy, int shields, int lifeSupport, int hull, int torpedos, int repairAndroids) {
		this(); //ruft den deafult Konstruktor auf, spart tippen
		setName(name);
		setEnergy(energy);
		setShields(shields);
		setLifeSupport(lifeSupport);
		setHull(hull);
		setTorpedos(torpedos);
		setRepairAndroid(repairAndroids);
	}
	
	//Setters
	public void setName(String name) {
		this.name = name;
	}
	public void setEnergy(int energy) {
		this.energy = energy;
	}
	public void setShields(int shields) {
		this.shields = shields;
	}
	public void setLifeSupport(int lifeSupport) {
		this.lifeSupport = lifeSupport;
	}
	public void setHull(int hull) {
		this.hull = hull;
	}
	public void setTorpedos(int torpedos) {
		this.torpedos = torpedos;
	}
	public void setRepairAndroid(int repairAndroids) {
		this.repairAndroids = repairAndroids;
	}
	public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		this.broadcastCommunicator = broadcastCommunicator;
	}
	public void setCargoManifest(ArrayList<Cargo> cargoManifest) {
		this.cargoManifest = cargoManifest;
	}
	
	//David Guettas
	public String getName() {
		return this.name;
	}
	public int getEnergy() {
		return this.energy;
	}
	public int getShields() {
		return this.shields;
	}
	public int getLifeSupport() {
		return this.lifeSupport;
	}
	public int getHull() {
		return this.hull;
	}
	public int getTorpedos() {
		return this.torpedos;
	}
	public int getRepairAndroids() {
		return this.repairAndroids;
	}
	public ArrayList<Cargo> getCargoManifest() {
		return this.cargoManifest;
	}
	
	//Methoden
	public void addCargo(Cargo newCargo) {
		this.cargoManifest.add(newCargo);
	}
	
	public void statusReport() {
		System.out.println("Shipname: " + getName());
		System.out.println("Energylevel: " + getEnergy() + "%");
		System.out.println("Shieldlevel: " + getShields() + "%");
		System.out.println("LifeSupport Level: " + getLifeSupport() + "%");
		System.out.println("Hull Integrity: " + getHull() + "%");
		System.out.println("Photontorpedos: " + getTorpedos());
		System.out.println("Repair Anddoids: " + getRepairAndroids());
	}
	
	public void readCargoManifest() {
		System.out.println("Cargo Manifest:");
		for(Cargo l : this.cargoManifest) {
			System.out.println("Cargoname: " + l.getTerm());
			System.out.println("Cargohold: " + l.getAmount());
		}
	}
	
	public void shootTorpedo(Spaceship spaceship) {
		if(this.torpedos == 0) {
			System.out.println("-=*click*=-");
		}else {
			this.torpedos--;
			System.out.println("Torpedo Launch detected!");
			spaceship.hitScan();
		}
	}
	
	public void shootCannons(Spaceship spaceship) {
		if(this.energy < 50) {
			System.out.println("-=*Click*=-");
		}else {
			this.energy = this.energy - 50;
			System.out.println("Phasercanon shot detected!");
			spaceship.hitScan();
		}
	}
	
	public void hitScan() {
		System.out.println(this.getName()+" has been hit.");
	}
	
	public void sendBroadcast(String message) {
		System.out.println(message);
	}
	

}//class
